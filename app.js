const express = require("express")
const app = express()

/**
 * RESTFUL TABLE 
 */

app.get('/results/:n1/:n2', (request, response) => {
    let n1 = parseInt(request.params.n1)
    let n2 = parseInt(request.params.n2)
    let result = n1 + n2 
    response.send(`${n1} + ${n2} = ${result}`)
})

app.post('/results/:n1/:n2', (request, response) => {
    let n1 = parseInt(request.params.n1)
    let n2 = parseInt(request.params.n2)
    let result = n1 * n2 
    response.send(`${n1} * ${n2} = ${result}`)
})

app.put('/results/:n1/:n2', (request, response) => {
    let n1 = parseInt(request.params.n1)
    let n2 = parseInt(request.params.n2)
    let result = n1 / n2 
    response.send(`${n1} / ${n2} = ${result}`)
})

app.patch('/results/:n1/:n2', (request, response) => {
    let n1 = parseInt(request.params.n1)
    let n2 = parseInt(request.params.n2)
    let result = n1 ** n2 
    response.send(`${n1} ^ ${n2} = ${result}`)
})

app.delete('/results/:n1/:n2', (request, response) => {
    let n1 = parseInt(request.params.n1)
    let n2 = parseInt(request.params.n2)
    let result = n1 - n2 
    response.send(`${n1} - ${n2} = ${result}`)
})

app.listen(4000, () => {
    console.log("App is up and running congrats!")
})
