# RESTful tables

Project to work with a rest table for web platforms class

## Installation

Use the package manager [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) to install the dependencies, including [Express](https://expressjs.com/).

```bash
npm i
```

Then use the following command to run the project 

```bash
node app.js
```

The app will run on port 4000.

## Usage

You can realize different math operations using different requests. 
```
http://localhost:4000/results/number/number
```
* GET -> Addition
* POST -> Multiplication
* PUT -> Division
* PATCH -> Power
* DELETE -> Minus


